# Listen verändern
---

## Elemente einer Liste hinzufügen

Die einfachste Art, Elemente einer Liste hinzuzufügen, ist der Aufruf der Funktion `liste.append(element)`. Dadurch wird das angegebene Element an die bestehende Liste angehängt:

``` python
wochentage = ["Mo", "Di", "Mi"]
wochentage.append("Do")
```

Allerdings kann ein Element mit Hilfe der Funktion `liste.insert(index, element)` an einer beliebigen Stelle in die Liste eingefügt werden. `index` gibt dabei an, an welcher Position das Element eingeschoben wird:

``` python
wochentage = ["Mo", "Mi", "Do", "Fr"]
wochentage.insert(1, "Di")
```

::: info Achtung
Wiederum muss beachtet werden, dass die Zählung bei `0` beginnt und der oben angegebene Index `1` somit bedeutet, dass das Element an zweiter Stelle eingefügt wird.
:::

## Elemente aus einer Liste entfernen

Wiederum gibt es verschiedene Möglichkeiten. Die einfachste liefert die Funktion `liste.remove(element)`. Sie tut genau das, was man erwartet und entfernt ein angegebenes Element (resp. das erste Auftreten) aus einer Liste:

``` python
wochentage = ["Mo", "Di", "Mi", "Do", "Fr"]
wochentage.remove("Di")
```

Soll ein Element an einer bestimmten Stelle aus der Liste entfernt werden, muss die Funktion `liste.pop(index)` verwendet werden. Dabei wird das Element als Resultat zurückgegeben, so dass dieses weiter verwendet werden kann:

``` python
wochentage = ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"]
ruhetag = wochentage.pop(6)
print("Am " + ruhetag + " habe ich frei!")
```

## Eine Liste in einer Schleife verändern

Elemente einer Liste können in einer Schleife systematisch verändert werden, beispielsweise wenn man jede Zahl in einer Liste quadrieren möchte:

``` python
liste = [1, 4, 13, 42, 27, 31, 5]

for i in range(0, len(liste)):
    liste[i] = liste[i] * liste[i]

print(liste)
```

::: exercise
#### :exercise: Aufgaben
1. Erstelle eine Liste mit den ersten 6 Monaten. Anschliessend
   - füge den August am Ende hinzu,
   - füge den Juli so in die Liste ein, dass er an der richtigen Stelle steht und gibt die Liste aus.
   - Entferne dann den Februar und
   - entferne auch den Monat mit Index `4` und gibt die Liste erneut aus.
2. Erstelle folgende Liste `["Hallo", "Hello", "Hallo", "Salut", "Ciao", "Hallo", "Hi", "Hallo"]` und entferne daraus in einer Schleife jedes Element, das gleich `"Hallo"` lautet.
3. Erstelle eine Liste mit den Zahlen von 1 bis 10 und gib jeweils nur jede zweite (also alle ungeraden Zahlen) aus.
***
1.
2. Diese Aufgabe soll dir zeigen, dass es problematisch ist, in einer Schleife Elemente aus einer Liste zu entfernen, da dann die Indizes nicht mehr stimmen. Es gibt also eine Fehlermeldung!
3. Du kannst hier die Operation `%` verwenden, um herauszufinden, ob die Zahl gerade ist (`if index % 2 == 0`).
***
1.
``` python
monate = ["Januar", "Februar", "März", "April", "Mai", "Juni"]
monate.append("August")
monate.insert(6, "Juli")
print(monate)
monate.remove("Februar")
monate.pop(4)
print(monate)
```
2.
``` python
liste = ["Hallo", "Hello", "Hallo", "Salut", "Ciao", "Hallo", "Hi", "Hallo"]

for i in range(0, len(liste)):
    if liste[i] == "Hallo":
        liste.pop(i)           # gefährlich: gibt eine Fehlermeldung!

print(liste)
```
3.
``` python
liste = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for i in range(0, len(liste)):
    if i % 2 == 0:
        print(liste[i])
```
:::

::: exercise
#### :extra: Zusatzaufgaben
1. Erstelle eine Liste mit den Wochentagen. Verändere die Liste in einer Schleife so, dass die Tage anschliessend in umgekehrter Reihenfolge in der Liste gespeichert sind.
2. Erweitere das Programm für den Notendurchschnitt so, dass der Benutzer die Noten eingeben kann. Er soll so lange nach Noten gefragt werden, bis er eine leere Eingabe tätigt. Rufe Anschliessend die Funktion auf, um den Durchschnitt zu berechnen und anzuzeigen.
3. Erstelle ein Programm, das die Fibonacci-Folge berechnet. Dabei sollen die ersten 100 Fibonacci-Zahlen in einer Liste gespeichert werden. Erst am Schluss soll die Liste ausgegeben werden.
***
1. Überlege dir ein immer gleiches Vorgehen (ein Element an einer bestimmten Stelle entfernen und an anderer Stelle wieder einfügen), um das Problem zu lösen.
2. Du musst die Eingabe mit `float()` in eine Fliesskommazahl umwandeln. Du brauchst eine Variable, in der du speicherst und überprüfst, ob der Benutzer noch Noten eingeben will.
3. Da die Liste wächst, brauchst du diesmal eine Schleife (z.B. eine `while`-Schleife), die zählt, wie viele Elemente die Liste bereits hat.
***
1.
``` python
wochentage = ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"]

for index in range(0, len(wochentage)):
    wochentage.insert(0, wochentage.pop(index))

print(wochentage)
```
2.
``` python
def durchschnitt(noten):
    notensumme = 0

    for note in noten:
        notensumme += note

    durchschnitt = notensumme / len(noten)
    return durchschnitt


notenliste = []
fertig = False

while not fertig:
    print("Gib eine Note ein (leer, um den Durchschnitt anzuzeigen)")
    eingabe = input()

    if eingabe == "":
        fertig = True
    else:
        note = float(eingabe)
        notenliste.append(note)

schnitt = durchschnitt(notenliste)
print("Der Notendurchschnitt beträgt " + str(schnitt))
```
3.
``` python
fibonacci = [0, 1]

i = 2
while i < 100:
    element = fibonacci[i - 2] + fibonacci[i - 1]
    fibonacci.append(element)
    i = i + 1

print(fibonacci)
print(len(fibonacci))
```

oder sogar ohne Index-Variable, indem wir von hinten auf die letzten beiden Elemente zugreifen:

``` python
fibonacci = [0,1]

while len(fibonacci) < 100:
    element = fibonacci[-2] + fibonacci[-1]
    fibonacci.append(element)

print(fibonacci)
print(len(fibonacci))
```
:::
