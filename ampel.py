"""
PROGRAMM        : Ampel.py
ZWECK           : Ein Script, das ein Lichtsignal simuliert
Autor           : Andreas Buerge
Datum           : 06.01.2021
"""

import tkinter as tk
from time import sleep

def schritt_1():
    canvas.itemconfigure(orange, state="normal")
    app.after(2000, schritt_2)

def schritt_2():
    canvas.itemconfigure(rot, state="hidden")
    canvas.itemconfigure(orange, state="hidden")
    canvas.itemconfigure(gruen, state="normal")
    app.after(5000, schritt_3)

def schritt_3():
    canvas.itemconfigure(gruen, state="hidden")
    canvas.itemconfigure(orange, state="normal")
    app.after(3000, schritt_4)

def schritt_4():
    canvas.itemconfigure(orange, state="hidden")
    canvas.itemconfigure(rot, state="normal")
    app.after(5000, schritt_1)
    

app = tk.Tk()
app.title("Ampel")
canvas = tk.Canvas(app, width=200, height=500, bg="lightblue")
canvas.pack()
canvas.create_rectangle(0, 0, 250, 500, fill="black")
rot = canvas.create_oval(50, 50, 150, 150, fill="red", state="normal")
orange = canvas.create_oval(50, 200, 150, 300, fill="orange", state="hidden")
gruen = canvas.create_oval(50, 350, 150, 450, fill="green", state="hidden")
app.after(5000, schritt_1)
app.mainloop()
