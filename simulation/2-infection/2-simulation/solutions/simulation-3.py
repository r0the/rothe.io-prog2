import pgzrun
import random
import math

TITLE = "Infektionssimulation"
WIDTH = 1200
HEIGHT = 600

ANZAHL = 100

# Zustände
GESUND = 0
INFIZIERT = 1
ERKRANKT = 2
GEHEILT = 3
GESTORBEN = 4

# Zeiten
INKUBATIONSZEIT = 6
KRANKHEITSZEIT = 14
ZYKLEN_PRO_TAG = 10

# Veranlagung
STERBEWAHRSCHEINLICHKEIT = 20
INFEKTIONSRISIKO = 50

# Ansteckungsdistanz
ANSTECKUNGSDISTANZ = 20

# Farben
FARBE_HINTERGRUND = (180, 180, 180)
FARBE_GESUND = (0, 138, 0)
FARBE_INFIZIERT = (255, 255, 0)
FARBE_ERKRANKT = (255, 0, 0)
FARBE_GEHEILT = (0, 0, 255)

# Grösse
RADIUS = 4

personen_x = []
personen_y = []
personen_dx = []
personen_dy = []
personen_zustand = []
personen_tage = []

# Anfangszustand
for i in range(0, ANZAHL):
    personen_x.append(random.randrange(0, WIDTH))
    personen_y.append(random.randrange(0, HEIGHT))
    personen_dx.append(random.randrange(-2, 2))
    personen_dy.append(random.randrange(-2, 2))
    personen_zustand.append(GESUND)
    personen_tage.append(0)

# erste Infizierte Person
zahl = random.randrange(0, ANZAHL)
personen_zustand[zahl] = INFIZIERT

# Statistik
anzahl_gesunde = ANZAHL - 1
anzahl_infizierte = 1
anzahl_erkrankte = 0
anzahl_geheilte = 0
anzahl_gestorbene = 0
ansteckungsglueck = 0


def draw():
    screen.fill(FARBE_HINTERGRUND)
    
    for i in range(0, ANZAHL):
        farbe = FARBE_GESUND
        
        if personen_zustand[i] == INFIZIERT:
            farbe = FARBE_INFIZIERT
        elif personen_zustand[i] == ERKRANKT:
            farbe = FARBE_ERKRANKT
        elif personen_zustand[i] == GEHEILT:
            farbe = FARBE_GEHEILT
        
        if personen_zustand[i] != GESTORBEN:
            screen.draw.filled_circle((personen_x[i], personen_y[i]), RADIUS, farbe)
    
    screen.draw.text("Gesunde: " + str(anzahl_gesunde), right = WIDTH, bottom = HEIGHT - 100)
    screen.draw.text("Infizierte: " + str(anzahl_infizierte), right = WIDTH, bottom = HEIGHT - 80)
    screen.draw.text("Erkrankte: " + str(anzahl_erkrankte), right = WIDTH, bottom = HEIGHT - 60)
    screen.draw.text("Geheilte: " + str(anzahl_geheilte), right = WIDTH, bottom = HEIGHT - 40)
    screen.draw.text("Gestorbene: " + str(anzahl_gestorbene), right = WIDTH, bottom = HEIGHT - 20)
    screen.draw.text("Ansteckungsglück: " + str(ansteckungsglueck), right = WIDTH, bottom = HEIGHT)


def update(zeitdifferenz):
    global anzahl_gesunde, anzahl_infizierte, anzahl_erkrankte, anzahl_geheilte, anzahl_gestorbene, ansteckungsglueck
    
    for i in range(0, ANZAHL):
        
        # Gesundheit prüfen
        if personen_zustand[i] == GESUND:
            for j in range(0, ANZAHL):
                distanz = math.sqrt((personen_x[i] - personen_x[j])**2 + (personen_y[i] - personen_y[j])**2)
                if (personen_zustand[j] == INFIZIERT or personen_zustand[j] == ERKRANKT) and distanz < ANSTECKUNGSDISTANZ:
                    infektion = random.randrange(0, 100)
                    if infektion < INFEKTIONSRISIKO:
                        personen_zustand[i] = INFIZIERT
                        anzahl_gesunde -= 1
                        anzahl_infizierte += 1
                        break
                    else:
                        ansteckungsglueck += 1
        elif personen_zustand[i] == INFIZIERT:
            if personen_tage[i] > INKUBATIONSZEIT * ZYKLEN_PRO_TAG:
                personen_tage[i] = 0
                personen_zustand[i] = ERKRANKT
                anzahl_infizierte -= 1
                anzahl_erkrankte += 1
            else:
                personen_tage[i] += 1
        elif personen_zustand[i] == ERKRANKT:
            if personen_tage[i] > KRANKHEITSZEIT * ZYKLEN_PRO_TAG:
                risiko = random.randrange(0, 100)
                if risiko < STERBEWAHRSCHEINLICHKEIT:
                    personen_zustand[i] = GESTORBEN
                    anzahl_erkrankte -= 1
                    anzahl_gestorbene += 1
                else:
                    anzahl_erkrankte -= 1
                    anzahl_geheilte += 1
                    personen_zustand[i] = GEHEILT
            else:
                personen_tage[i] += 1
        
        # bewegen
        personen_x[i] += personen_dx[i]
        personen_y[i] += personen_dy[i]
    
        if personen_x[i] < 0 or personen_x[i] > WIDTH:
            personen_dx[i] = personen_dx[i] * -1
        
        if personen_y[i] < 0 or personen_y[i] > HEIGHT:
            personen_dy[i] = personen_dy[i] * -1


pgzrun.go()