# Modell
---

Wir verwenden für diese Reihe ein sehr einfaches Modell eines Virus. In den Aufgaben werden Variationen dieses Modells vorgeschlagen. Ihr solltet darauf aufbauend versuchen, euer eigenes Modell zu entwickeln und zu implementieren.

## Das Modell für die Bewegung in der Umgebung

In unserem Modell sollen sich 100 Personen in einem Raum frei und zufällig bewegen. Die Infizierung und Erkrankung geschieht gemäss einem zustandsbasierten Modell, das im nächsten Schritt beschrieben wird.

Die **Umgebung** wird als Rechteck modelliert, dessen Grösse wir in Abhängigkeit von der Anzahl der Personen wählen. Jede Person soll in diesem Rechteck zufällig platziert werden. Dabei sollte die Wahrscheinlichkeit der Zuordnung zu einer Position innerhalb des Rechteckes gleichverteilt sein.

Die **Bewegung** der Individuen soll gleichförmig erfolgen, d. h. die Personen bewegen sich mit konstanter Geschwindigkeit immer in die gleiche Richtung, am Rand des Rechtecks «prallen sie ab» (Einfallswinkel gleich Ausfallswinkel). Alle Personen bewegen sich mit gleicher Geschwindigkeit und weichen einander nicht aus.

## Das Modell für die Infektion

Wie bereits erwähnt modellieren wir ein sehr einfaches Modell eines eher harmlosen Virus. Wir gehen in unserem Modell davon aus, dass sich eine Person jederzeit in genau einem der folgenden vier Zustände befindet:

- gesund
- infiziert
- erkrankt
- geheilt

Die Infizierung erfolgt **immer sofort** bei einem Kontakt mit einer bereits **infizierten** oder **erkrankten** Person. Nach der Infizierung dauert es exakt 6 Tage, bis die Krankheit ausbricht. Nach 14 weiteren Tagen gilt die Person als geheilt. Eine erneute Erkrankung ist nicht mehr möglich. Dieser Teil der Modellierung ist im folgenden Zustandsdiagramm dargestellt.

![Zustandsdiagramm Krankheitsverlauf](./images/state-diagram-infection.svg)

Unser Modell enthält noch keine Massnahmen zur Reduzierung oder Vermeidung von Kontakten wie Ausgangssperren oder Quarantäne.
